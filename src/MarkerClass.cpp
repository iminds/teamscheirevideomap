//
//  MarkerClass.cpp
//  testVideoMap
//
//  Created by Jan Everaert on 05/08/2019.
//

#include "MarkerClass.hpp"


void MarkerClass::setup(int markerId, float oX, float oY) {
    offsetX = oX;
    offsetY = oY;
    id = markerId;
}
