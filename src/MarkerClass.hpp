//
//  MarkerClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 05/08/2019.
//

#ifndef MarkerClass_hpp
#define MarkerClass_hpp

#include <stdio.h>
#include "ofMain.h"

class MarkerClass {
public:
    
    void setup(int id, float oX, float oY);
    
    int id;
    float offsetX;
    float offsetY;
};

#endif /* MarkerClass_hpp */
