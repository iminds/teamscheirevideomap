//
//  SurfaceClass.cpp
//  testVideoMap
//
//  Created by Jan Everaert on 30/07/2019.
//

#include "SurfaceClass.hpp"

float deg = 0;
void SurfaceClass::setup(string ha, ofVideoPlayer v, float w, float h, float tX, float tY, float tW, float tH, vector<MarkerClass> markers) {
    texture.allocate(ofGetWidth(), ofGetHeight());
    
    texture.begin();
    ofClear(0, 0, 0);
    texture.end();
    
    tStartX = tX;
    tStartY = tY;
    
    tWidth = tW;
    tHeight = tH;
    
    width = w;
    height = h;
    
    handler = ha;
    
    markerList = markers;
    
    videoProjection.set(800, 600, 2, 2);
    videoProjection.setPosition(ofGetWindowWidth()/2, ofGetWindowHeight()/2, 0);
    
}

void SurfaceClass::update(ofVideoPlayer v) {
    
    texture.begin();
    ofClear(255, 255, 255);
    ofPushMatrix();
    v.draw(0, 0,  800,  600);
    ofPopMatrix();
    texture.end();
    
}

int SurfaceClass::isMarkerIdInList(int markerId) {
    for(int i = 0; i < markerList.size(); i++) {
        if(markerList[i].id == markerId) {
            return markerList[i].id;
        }
    }
    return -1;
}

void SurfaceClass::draw(ofVec3f position) {
    deg+=.2;
    
    ofTexture t = texture.getTexture();
    ofPushMatrix();
    ofTranslate(position);
    
    ofRotateXDeg(deg);
    ofRotateYDeg(deg/2);
    ofRotateZDeg(deg/3);
    
    t.drawSubsection(-width / 2, -height / 2, width, height, tStartX, tStartY, tWidth, tHeight);
    
    ofPopMatrix();
    
    ofPushMatrix();
    ofDrawBitmapString(handler, position.x, position.y + 200);
    ofPopMatrix();
}


void SurfaceClass::drawInPlace(float scale, int markerId, ofMatrix4x4 matrix) {
    
//    history.push_back(matrix);
    
//    ofMatrix4x4 total;
//    for(int i = 0; i < history.size(); i++) {
//
//    }
    
    
    
    ofTexture t = texture.getTexture();
    ofPushMatrix();
    
    
    ofScale(scale/100);
    
    float tx;
    float ty;
    for(int i = 0; i < markerList.size(); i++) {
        if(markerList[i].id == markerId) {
            tx = markerList[i].offsetX;
            ty = markerList[i].offsetY;
        }
    }
    ofTranslate(tx, ty);

    t.drawSubsection(tx, ty, width, height, tStartX, tStartY, tWidth, tHeight);
    
    ofPopMatrix();
    
}
