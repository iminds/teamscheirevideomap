//
//  InterfaceClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 05/08/2019.
//

#ifndef InterfaceClass_hpp
#define InterfaceClass_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxGui.h"


class InterfaceClass {
    
public:
    void setup();
    void update();
    void draw();
    
    void save();
    void load();
    
    int OFFSET_AMOUNT = 50;
    float SCALE_AMOUNT = 2;
    bool displayUI;
    
    ofxPanel gui;
    ofParameter<float> scale;
    ofParameter<float> threshold;
    ofParameter<int> frameWait;
    ofxToggle inSpace;
    ofxToggle showVideo;
    ofxToggle useThreshold;
    ofxToggle useSimulated;
    ofxButton saveSettings;
    ofxButton loadSettings;
    
    ofParameter<float> viewOffsetY;
    ofParameter<float> viewOffsetX;
    ofParameter<float> viewOffsetZ;
    ofParameter<float> viewWidthScale;
    ofParameter<float> viewHeightScale;
    ofParameter<float> viewDepthScale;
};


#endif /* InterfaceClass_hpp */
