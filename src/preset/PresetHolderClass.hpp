//
//  PresetHolderClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 09/08/2019.
//

#ifndef PresetHolderClass_hpp
#define PresetHolderClass_hpp

#include <stdio.h>
#include "ofMain.h"
#include "ofxXmlSettings.h"
#include "PresetItemClass.hpp"
#include "PresetFileClass.hpp"

class PresetHolderClass {
public:
    void setup();
    void update(ofVideoPlayer v);
    void draw();
    void handleKey(int key);
    ofxXmlSettings xmlPresets;
    
    vector<PresetFileClass> presetFiles;
    int currentPreset = 0;
    
    ofEasyCam cam;
};
#endif /* PresetHolderClass_hpp */
