//
//  PresetItemClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 09/08/2019.
//

#ifndef PresetItemClass_hpp
#define PresetItemClass_hpp

#include <stdio.h>
#include "ofMain.h"


class PresetItemClass {
public:
    void setup(string ha, int w, int h, int xO, int yO, int zO, float xR, float yR, float zR, int vSX, int VSY, int vW, int vH);
    void update(ofVideoPlayer v);
    void draw();
    
    
    ofPlanePrimitive videoProjection;
    ofFbo texture;
    
    string handle;
    int width;
    int height;
    int xOffset;
    int yOffset;
    int zOffset;
    float xRotation;
    float yRotation;
    float zRotation;
    
    
    int vStartX;
    int vStartY;
    int vWidth;
    int vHeight;
    
    
    ofFbo mask;
    ofImage img;

    
};
#endif /* PresetItemClass_hpp */
