//
//  PresetItemClass.cpp
//  testVideoMap
//
//  Created by Jan Everaert on 09/08/2019.
//

#include "PresetItemClass.hpp"


void PresetItemClass::setup(string ha = "test", int w = 200, int h = 200, int xO = 50, int yO = 50, int zO = 50, float xR = 0, float yR = 0, float zR = 0, int vSX = 0, int vSY = 0, int vW = 200, int vH = 200) {
    handle = ha;
    width = w;
    height =h;
    xOffset = xO;
    yOffset = yO;
    zOffset = zO;
    xRotation = xR;
    yRotation = yR;
    zRotation = zR;
    
    vStartX = vSX;
    vStartY = vSY;
    vWidth = vW;
    vHeight = vH;
    
    
    
    ofEnableAlphaBlending();
    img.load("testAlpha.jpg");
    img.setImageType(OF_IMAGE_COLOR_ALPHA);
    
    texture.allocate(width, height, GL_RGBA);
    texture.begin();
    ofClear(0, 0, 0, 0);
    texture.end();

    
}

void PresetItemClass::update(ofVideoPlayer v) {
    texture.getTexture().setAlphaMask(img.getTexture());
    texture.begin();
    ofClear(0, 0, 0, 0);
    ofPushMatrix();
    v.draw(0, 0,  width,  height);
    ofPopMatrix();
    texture.end();
    
}

void PresetItemClass::draw() {
    
    ofDrawAxis(200);

    ofPushMatrix();
    ofTranslate(xOffset, yOffset, zOffset);
    ofRotateXDeg(xRotation);
    ofRotateYDeg(yRotation);
    ofRotateZDeg(zRotation);
    ofPushStyle();
    ofSetColor(255, 255, 255, 200);
    ofNoFill();
    ofDrawRectangle(-width / 2, -height / 2, width, height);
    ofTexture t = texture.getTexture();
    
    
    
    ofEnableAlphaBlending();
    t.drawSubsection(-width / 2, -height / 2, width, height, vStartX, vStartY, vWidth, vHeight);
    ofDisableAlphaBlending();
    ofPopStyle();
    ofPopMatrix();
}
