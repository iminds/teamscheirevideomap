//
//  PresetFileClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 09/08/2019.
//

#ifndef PresetFileClass_hpp
#define PresetFileClass_hpp

#include <stdio.h>
#include "ofMain.h"
#include "PresetItemClass.hpp"

class PresetFileClass {
public:
    void setup(vector<PresetItemClass> presetI);
    void update(ofVideoPlayer v);
    void draw();
    
    vector<PresetItemClass> presetItems;
    
};
#endif /* PresetFileClass_hpp */
