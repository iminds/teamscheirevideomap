//
//  SurfaceClass.hpp
//  testVideoMap
//
//  Created by Jan Everaert on 30/07/2019.
//

#ifndef SurfaceClass_hpp
#define SurfaceClass_hpp

#include <stdio.h>
#include "ofMain.h"
#include "MarkerClass.hpp"
#include "ofxXmlSettings.h"
class SurfaceClass {
    
public:
    void setup(string ha, ofVideoPlayer v, float w, float h, float tX, float tY, float tW, float tH, vector<MarkerClass> markers);
    void update(ofVideoPlayer v);
    void draw(ofVec3f position);
    void drawInPlace(float scale, int markerId, ofMatrix4x4 matrix);
    
    int isMarkerIdInList(int markerId);
    
    ofPlanePrimitive videoProjection;
    ofFbo texture;
    ofMesh mesh;
    
    
    float tStartX;
    float tStartY;
    float tWidth;
    float tHeight;
    
    float width;
    float height;
    
    vector<MarkerClass> markerList;
    
    vector<ofMatrix4x4> history;
    
    string handler;
};

#endif /* SurfaceClass_hpp */
