//
//  InterfaceClass.cpp
//  testVideoMap
//
//  Created by Jan Everaert on 05/08/2019.
//

#include "InterfaceClass.hpp"

void InterfaceClass::setup() {
    gui.setup();
    
    gui.add(scale.set("scale", .2, 0, 1));
    gui.add(useThreshold.setup("use threshold", true));
    gui.add(threshold.set("threshold (i/k)", 200, 0, 255));
    gui.add(frameWait.set("frame wait (o/l)", 1, 1, 240));
    gui.add(inSpace.setup("space/water", true));
    gui.add(showVideo.setup("showVideo", true));
    gui.add(useSimulated.setup("useSimulated", true));
    
    
    gui.add(viewOffsetX.set("viewOffsetX", 0, -OFFSET_AMOUNT, OFFSET_AMOUNT));
    gui.add(viewOffsetY.set("viewOffsetY", 0, -OFFSET_AMOUNT, OFFSET_AMOUNT));
    gui.add(viewOffsetZ.set("viewOffsetZ", 0, -OFFSET_AMOUNT, OFFSET_AMOUNT));
    gui.add(viewWidthScale.set("width scale", 1, 0, SCALE_AMOUNT));
    gui.add(viewHeightScale.set("height scale", 1, 0, SCALE_AMOUNT));
    gui.add(viewDepthScale.set("depth scale", 1, 0, SCALE_AMOUNT));
    
    gui.add(saveSettings.setup("save settings"));
    gui.add(loadSettings.setup("load settings"));
    
    saveSettings.addListener(this, &InterfaceClass::save);
    loadSettings.addListener(this, &InterfaceClass::load);

}

void InterfaceClass::update() {
    
}

void InterfaceClass::draw() {
    if(displayUI) {
        gui.draw();
    }
}

void InterfaceClass::save() {
    gui.saveToFile("settings.xml");
}
void InterfaceClass::load() {
    gui.loadFromFile("settings.xml");
}
