#include "ofApp.h"
#include "ofxCv.h"


void drawMarker(float size, const ofColor & color, int id){
    ofDrawAxis(size);
    ofPushMatrix();
    ofPushStyle();
    ofTranslate(0,size*0.5,0);
    ofFill();
    ofSetColor(color,50);
    ofDrawBox(size);
    ofNoFill();
    ofSetColor(color);
    ofDrawBox(size);
    ofDrawBitmapString(id, 0, 0);
    ofPopStyle();
    ofPopMatrix();
    
}
//--------------------------------------------------------------
void ofApp::setup(){
    ofBackground(0);
    
    video.loadMovie("benchmark.mov");
    video.play();

    sound.load("space.wav");
    sound.setVolume(0);
    sound.play();

    // setting aruco track
    grabber.setDeviceID(1);
    grabber.initGrabber(640, 480);
    thresh.allocate(640, 480, OF_IMAGE_GRAYSCALE);

    
    trackVideo = &grabber;
    
    aruco.setup("intrinsics.int", trackVideo->getWidth(), trackVideo->getHeight());
    ofEnableAlphaBlending();


    ofSetFrameRate(24);
//    ofHideCursor();
    ofEnableAlphaBlending();
    
    setupSurfaces(video);
    
    UI.setup();
    UI.inSpace.addListener(this, &ofApp::inputChanged);
    
    presets.setup();
    
    

}

void ofApp::setupSurfaces(ofVideoPlayer v) {
    
    if(xml.loadFile("setup.xml")){
        xml.pushTag("setup");
        
        int numberOfSurfaces = xml.getNumTags("surface");
        for(int i = 0; i < numberOfSurfaces; i++){
            xml.pushTag("surface", i);
            SurfaceClass surface;
            
            string name = xml.getValue("name", "default");
            int w = xml.getValue("video:width", 100);
            int h = xml.getValue("video:height", 200);
            int x1 = xml.getValue("video:x1", 0);
            int y1 = xml.getValue("video:y1", 0);
            int x2 = xml.getValue("video:x2", 100);
            int y2 = xml.getValue("video:y2", 100);
            
            vector<MarkerClass> markers;
            xml.pushTag("markers");
            int numberOfMarkers = xml.getNumTags("marker");
            for(int j = 0; j < numberOfMarkers; j++){
                xml.pushTag("marker", j);
                MarkerClass marker;
                marker.setup(xml.getValue("id", 0), xml.getValue("offsetX", 0), xml.getValue("offsetY", 0));
                markers.push_back(marker);
                xml.popTag();
            }
            xml.popTag();
            surface.setup("ceiling", video, w, h, x1, y1, x2, y2, markers);

            surfaces.push_back(surface);
            
            xml.popTag();
        }
        xml.popTag();
    }
    else{
        ofLogError("Position file did not load!");
    }
}

void ofApp::exit(){
    UI.inSpace.removeListener(this, &ofApp::inputChanged);
}
//--------------------------------------------------------------
void ofApp::update(){

    
    if(ofGetFrameNum() % UI.frameWait == 0) {
        takeFrame = true;
    }
    else {
        takeFrame = false;
    }
    
    
    video.update();
    trackVideo->update();
    
    
    if(trackVideo->isFrameNew()){
        
        if(USE_CV) {
            ofxCv::convertColor(trackVideo->getPixels(), thresh, CV_RGB2GRAY);
            ofxCv::threshold(thresh, UI.threshold);
            if(takeFrame) {
                aruco.detectBoards(thresh);
            }
        }
        else {
            if(takeFrame) {
                aruco.detectBoards(trackVideo->getPixels());
            }
        }

    }
    thresh.update();

    
    for(int i = 0; i < surfaces.size(); i++) {
        surfaces.at(i).update(video);
    }
    
    UI.update();
    presets.update(video);
    
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    
    if(UI.useSimulated && USE_SIM) {
        presets.draw();
    } else {
        if(UI.showVideo) {
            if(USE_CV) {
                thresh.draw(0, 0, ofGetWidth(), ofGetHeight());
            }
            else {
                trackVideo->draw(0, 0, ofGetWidth(), ofGetHeight());
            }
        }
        vector<aruco::Marker> markers = aruco.getMarkers();
        for(int i = 0; i < markers.size(); i++) {
            aruco.begin(i);
            ofMatrix4x4 matrix = aruco.getProjectionMatrix();
            drawMarker(0.15, ofColor::white, markers.at(i).id);
            for(int j = 0; j < surfaces.size(); j++) {
                if(surfaces.at(j).isMarkerIdInList(markers.at(i).id) != -1) {
                    surfaces.at(j).drawInPlace(UI.scale, markers.at(i).id, matrix);
                }
            }
            aruco.end();
        }
        
    }
    
    
    
    ofImage test;
    test.grabScreen(0, 0, ofGetWidth(), ofGetHeight());
    
    
    
    ofClear(0, 0, 0);
    ofPushMatrix();
    ofTranslate(UI.viewOffsetX, UI.viewOffsetY, UI.viewOffsetZ);
    ofScale(UI.viewHeightScale, UI.viewWidthScale, UI.viewDepthScale);
    
    test.draw(0, 0);
    ofPopMatrix();
    UI.draw();
    
    ofDrawBitmapString(floor(ofGetFrameRate()), ofGetWidth() - 50, 10);
    
    
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
//    if(USE_NUMPAD) {
//        keymap = {'i', 'k', 'I', 'K', 'o', 'l', 'f', 'h', 'v', 's'};
//    }
//    else {
//        keymap = {'+', '-', ' ', ' ', '/', '*', ' ', '9', '0', OF_KEY_RETURN};
//    }
    
    if(key == 'i' || key == '+') {
        UI.threshold.set(UI.threshold + 20);
    }
    else if(key == 'k' || key == '-') {
        UI.threshold.set(UI.threshold - 20);
        if(UI.threshold < 0) {
            UI.threshold.set(0);
        }
    }
    else if(key == 'I') {
        UI.threshold.set(UI.threshold + 2);
    }
    else if(key == 'K') {
        UI.threshold.set(UI.threshold - 2);
        if(UI.threshold < 0) {
            UI.threshold.set(0);
        }
    }
    else if(key == 'o' || key == '*') {
        UI.frameWait.set(UI.frameWait + 5);
    }
    else if((key == 'l' || key == '/') && UI.frameWait) {
        UI.frameWait.set(UI.frameWait - 5);
        if(UI.frameWait < 1) {
            UI.frameWait.set(1);
        }
    }
    
    else if(key == 'f') {
        string filename = "saves/screen";
        string frameNum =  ofToString(ofGetFrameNum());
        filename = filename + frameNum + ".png";
        ofSaveScreen(filename);
        
    }
    else if(key == 'h' || key == '9') {
        UI.displayUI = !UI.displayUI;
    }
    
    else if(key == 'v' || key == '0') {
        // change interface
        // add more in case of more scenarios;
        UI.inSpace = !UI.inSpace;
    }
    else if(key == 's' || key == '8') {
        USE_SIM = !USE_SIM;
    }
    else {
        // most likely number input, load the file in the presets
        presets.handleKey(key);
    }
    
}

void ofApp::inputChanged(bool & inSpace) {
    if(inSpace) {
        video.loadMovie("benchmark.mov");
        sound.load("space.wav");
        video.play();
        sound.play();
        
    } else {
        video.loadMovie("benchmark.mov");
        sound.load("water.wav");
        video.play();
        sound.play();
        
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
